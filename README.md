# Santorini

<h3><a href="https://dragonblade.gitlab.io/santorini">Analysis board</a></h3>

Simulator and analysis board for the [Santorini](https://en.wikipedia.org/wiki/Santorini_(game)) board game.
The analysis board uses [Monte Carlo Tree Search](https://en.wikipedia.org/wiki/Monte_Carlo_tree_search) for move suggestions.

## Development

To build and run the analysis board locally, [Trunk](https://trunkrs.dev/) must be installed on your system.

```sh
trunk serve
```

## License

This project is distributed under the terms of the MIT license.

See the [LICENSE](LICENSE) file for more information.
