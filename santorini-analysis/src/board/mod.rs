use std::rc::Rc;

use yew::prelude::*;

use santorini_game::{Move, State};

pub struct Board {
    state: Rc<State>,
    _listener: ContextHandle<Rc<State>>,
    move_src: Option<u8>,
    move_dst: Option<u8>,
}

pub enum BoardMsg {
    StateChanged(Rc<State>),
    SelectSrc(u8),
    SelectDst(u8),
    SelectMove(Move),
    SelectUndo,
}

#[derive(PartialEq, Properties)]
pub struct BoardProps {
    pub on_move: Callback<Move>,
    pub on_wheel: Callback<WheelEvent>,
}

impl Component for Board {
    type Message = BoardMsg;
    type Properties = BoardProps;

    fn create(ctx: &Context<Self>) -> Self {
        let (state, _listener) = ctx
            .link()
            .context::<Rc<State>>(ctx.link().callback(BoardMsg::StateChanged))
            .expect("context to be set");

        Self {
            state,
            _listener,
            move_src: None,
            move_dst: None,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            BoardMsg::StateChanged(state) => {
                self.state = state;
                true
            }
            BoardMsg::SelectSrc(src) => {
                self.move_src = Some(src);
                true
            }
            BoardMsg::SelectDst(dst) => {
                self.move_dst = Some(dst);
                true
            }
            BoardMsg::SelectMove(mov) => {
                self.move_src.take();
                self.move_dst.take();
                ctx.props().on_move.emit(mov);
                true
            }
            BoardMsg::SelectUndo => {
                if self.move_dst.take().is_none() {
                    self.move_src.take();
                }
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
            <div
                class="board"
                onwheel={ctx.props().on_wheel.clone()}
            >
                { self.view_tiles(ctx) }
            </div>
        }
    }
}

impl Board {
    fn view_tiles(&self, ctx: &Context<Self>) -> Html {
        self.state
            .view_buildings()
            .into_iter()
            .zip(self.state.view_players().into_iter())
            .enumerate()
            .map(|(i, (build, player))| {
                let moves = self.state.legal_moves_vec();

                let (class, onclick) =
                    if self.move_src.is_none() && moves.iter().any(|m| m.src == i as u8) {
                        (
                            Some("hover"),
                            Some(ctx.link().callback(move |_| BoardMsg::SelectSrc(i as u8))),
                        )
                    } else if self.move_dst.is_none()
                        && moves
                            .iter()
                            .any(|m| Some(m.src) == self.move_src && m.dst == i as u8)
                    {
                        (
                            Some("highlight"),
                            Some(ctx.link().callback(move |_| BoardMsg::SelectDst(i as u8))),
                        )
                    } else if let Some(mov) = moves.iter().find(|m| {
                        Some(m.src) == self.move_src
                            && Some(m.dst) == self.move_dst
                            && m.build == i as u8
                    }) {
                        let mov = *mov;
                        (
                            Some("highlight"),
                            Some(ctx.link().callback(move |_| BoardMsg::SelectMove(mov))),
                        )
                    } else if self.move_src == Some(i as u8) || self.move_dst == Some(i as u8) {
                        (
                            Some("selected"),
                            Some(ctx.link().callback(|_| BoardMsg::SelectUndo)),
                        )
                    } else {
                        (None, None)
                    };

                html! {
                    <div
                        class={classes!("tile", class)}
                        onclick={onclick}
                    >
                        {
                            build.iter().enumerate().filter(|(_, t)| **t).map(|(l, _)|
                                html! {
                                    <div class={classes!("building", format!("l-{}", l))}>
                                    </div>
                                }
                            ).collect::<Html>()
                        }
                        <div class={classes!("player", player.map(|p| format!("p-{}", p)))}></div>
                    </div>
                }
            })
            .collect::<Html>()
    }
}
