use yew::prelude::*;

use santorini_game::{Move, TreeNode};

pub struct History {}

#[derive(PartialEq, Properties)]
pub struct HistoryProps {
    pub tree_root: TreeNode,
    pub tree_current: TreeNode,
    pub on_node: Callback<TreeNode>,
}

impl Component for History {
    type Message = ();
    type Properties = HistoryProps;

    fn create(_ctx: &Context<Self>) -> Self {
        Self {}
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
            <div class="history">
                { self.view_tree(ctx) }
            </div>
        }
    }
}

impl History {
    fn view_tree(&self, ctx: &Context<Self>) -> Html {
        let mut elem = vec![];
        let mut turn = 0;

        let mut current = Some(ctx.props().tree_root.clone());
        while let Some(node) = current.take() {
            let children = node.children();
            let mut children = children.iter();

            if let Some((mov, next)) = children.next() {
                if mov.player == 0 || elem.is_empty() {
                    turn += 1;
                    elem.push(html! { <div class="num">{ format!("{}", turn) }</div> });
                    if mov.player != 0 {
                        elem.push(html! { <div class="dots">{ "..." }</div> });
                    }
                }

                elem.push(self.view_move(ctx, mov, next));
                current = Some(next.clone());
            }

            let mut first_player = false;
            let mut sides = vec![];
            for (mov, next) in children {
                if mov.player == 0 {
                    first_player = true;
                }
                sides.push(self.view_side_tree(ctx, mov, next, turn));
            }
            if sides.len() > 0 {
                if first_player {
                    elem.push(html! { <div class="dots">{ "..." }</div> });
                }
                elem.push(html! { <div class="sub">{ sides.into_iter().collect::<Html>() }</div> });
                if first_player {
                    elem.push(html! { <div class="num">{ format!("{}", turn) }</div> });
                    elem.push(html! { <div class="dots">{ "..." }</div> });
                }
            }
        }

        elem.into_iter().collect::<Html>()
    }

    fn view_side_tree(
        &self,
        ctx: &Context<Self>,
        mov: &Move,
        node: &TreeNode,
        mut turn: usize,
    ) -> Html {
        let mut elem = vec![
            html! {
                <>
                    <span class="num">{ format!("{}.", turn) }</span>
                    if mov.player != 0 {
                        <span class="dots">{ "..." }</span>
                    }
                </>
            },
            self.view_side_move(ctx, mov, node),
        ];
        let mut sides = vec![];

        let mut current = Some(node.clone());
        while let Some(node) = current.take() {
            let children = node.children();
            let mut children = children.iter();

            if let Some((mov, next)) = children.next() {
                if mov.player == 0 {
                    turn += 1;
                    elem.push(html! { <span class="num">{ format!("{}.", turn) }</span> });
                }

                elem.push(self.view_side_move(ctx, mov, next));
                current = Some(next.clone());
            }

            for (mov, next) in children {
                sides.push(self.view_side_tree(ctx, mov, next, turn));
            }
        }

        html! {
            <div class="line">
                { elem.into_iter().collect::<Html>() }
                { sides.into_iter().collect::<Html>() }
            </div>
        }
    }

    fn view_move(&self, ctx: &Context<Self>, mov: &Move, node: &TreeNode) -> Html {
        let node = node.clone();
        html! {
            <div
                class={classes!(
                    "move",
                    (ctx.props().tree_current == node).then_some("active")
                )}
                onclick={ctx.props().on_node.reform(move |_| node.clone())}
            >
                <span class={classes!("player", format!("p-{}", mov.player))}></span>
                <span>{ direction_to_arrow(mov.src, mov.dst) }</span>
                <span class="build">{ '\u{1F3E0}' }</span>
                <span>{ direction_to_arrow(mov.dst, mov.build) }</span>
            </div>
        }
    }

    fn view_side_move(&self, ctx: &Context<Self>, mov: &Move, node: &TreeNode) -> Html {
        let node = node.clone();
        html! {
            <span
                class={classes!(
                    "move",
                    (ctx.props().tree_current == node).then_some("active")
                )}
                onclick={ctx.props().on_node.reform(move |_| node.clone())}
            >
                { direction_to_arrow(mov.src, mov.dst) }
                { direction_to_arrow(mov.dst, mov.build) }
            </span>
        }
    }
}

fn direction_to_arrow(src: u8, dst: u8) -> char {
    match (dst as i8) - (src as i8) {
        -6 => '\u{2196}',
        -5 => '\u{2191}',
        -4 => '\u{2197}',
        -1 => '\u{2190}',
        1 => '\u{2192}',
        4 => '\u{2199}',
        5 => '\u{2193}',
        6 => '\u{2198}',
        _ => unreachable!(),
    }
}
