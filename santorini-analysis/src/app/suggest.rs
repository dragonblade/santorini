use std::rc::Rc;

use yew::prelude::*;
use yew_agent::{worker::WorkerBridge, Spawnable};

use super::StateChange;
use crate::worker::{Worker, WorkerInput, WorkerOutput};
use santorini_game::{Move, State, Suggestion};

pub struct Suggest {
    state: Rc<State>,
    _listener: ContextHandle<Rc<State>>,
    bridge: WorkerBridge<Worker>,
    enabled: bool,
    stopped: bool,
    suggestions: Vec<Suggestion>,
    progress: f64,
    speed: f64,
    depth: usize,
    reset: Option<State>,
}

pub enum SuggestMsg {
    StateChanged(Rc<State>),
    MoveMultiple(usize, usize),
    WorkerMsg(WorkerOutput),
    WorkerToggle,
}

#[derive(PartialEq, Properties)]
pub struct SuggestProps {
    pub on_change: Callback<StateChange>,
}

impl Component for Suggest {
    type Message = SuggestMsg;
    type Properties = SuggestProps;

    fn create(ctx: &Context<Self>) -> Self {
        let (state, _listener) = ctx
            .link()
            .context::<Rc<State>>(ctx.link().callback(SuggestMsg::StateChanged))
            .expect("context to be set");

        let cb = {
            let link = ctx.link().clone();
            move |e| link.send_message(Self::Message::WorkerMsg(e))
        };
        let bridge = Worker::spawner().callback(cb).spawn("/worker.js");

        Self {
            state,
            _listener,
            bridge,
            enabled: false,
            stopped: true,
            suggestions: vec![],
            progress: 0f64,
            speed: 0f64,
            depth: 0,
            reset: None,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            SuggestMsg::StateChanged(state) => {
                *Rc::make_mut(&mut self.state) = (*state).clone();

                self.suggestions.clear();
                if self.enabled {
                    if self.stopped {
                        self.stopped = false;
                        self.bridge.send(WorkerInput::New((*self.state).clone()));
                    } else {
                        self.reset = Some((*self.state).clone());
                    }
                }
                true
            }
            SuggestMsg::MoveMultiple(idx_sug, idx_next) => {
                if let Some(sug) = self.suggestions.get(idx_sug) {
                    let mut moves = vec![sug.mov];
                    moves.extend(sug.next.iter().take(idx_next + 1));
                    ctx.props().on_change.emit(StateChange::Multiple(moves));
                }
                false
            }
            SuggestMsg::WorkerMsg(msg) => match msg {
                WorkerOutput::Results {
                    suggestions,
                    progress,
                    speed,
                    depth,
                } => {
                    if self.enabled && self.reset.is_none() {
                        self.suggestions = suggestions;
                        self.progress = progress;
                        self.speed = speed;
                        self.depth = depth;

                        self.bridge.send(WorkerInput::Continue);

                        true
                    } else if let Some(state) = self.reset.take() {
                        self.bridge.send(WorkerInput::New(state));

                        false
                    } else {
                        false
                    }
                }
                WorkerOutput::Stopped => {
                    self.stopped = true;
                    true
                }
            },
            SuggestMsg::WorkerToggle => {
                self.enabled = !self.enabled;
                if self.enabled {
                    self.stopped = false;
                    self.bridge.send(WorkerInput::New((*self.state).clone()));
                } else {
                    self.stopped = true;
                }

                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
            <div class="suggest">
                <hgroup>
                    <h5>
                        <span>{ "Suggestions" }</span>
                        <span class="check">
                            <input
                                type="checkbox"
                                role="switch"
                                checked={self.enabled}
                                onchange={ctx.link().callback(|_| SuggestMsg::WorkerToggle)}
                            />
                        </span>
                    </h5>
                    <h6>
                        <span>{ format!("Depth {}", self.depth) }</span>
                        if self.enabled && !self.stopped {
                            <span>{ format!(", {:.0}k runs/s", self.speed / 1_000f64) }</span>
                        }
                    </h6>
                </hgroup>
                if (self.enabled || !self.suggestions.is_empty()) && self.reset.is_none() {
                    <progress value={ format!("{:.0}", self.progress) } max="100"></progress>
                }
                { self.view_suggestions(ctx) }
            </div>
        }
    }
}

impl Suggest {
    fn view_suggestions(&self, ctx: &Context<Self>) -> Html {
        if self.suggestions.is_empty() {
            return html! { <></> };
        }

        html! {
            <div class="moves">
                {
                    self.suggestions.iter().enumerate().take(5).map(|(idx_sug, sug)| {
                        html! {
                            <>
                                <div class="num">{ format!("{:.2}", sug.score) }</div>
                                <div class="num">{ format!("{:.1}%", sug.win * 100f64) }</div>
                                { self.view_move(ctx, sug.mov) }
                                <div class="next">
                                    {
                                        sug.next.iter().enumerate().map(|(idx_m, m)|
                                            self.view_side_move(ctx, m, idx_sug, idx_m)
                                        ).collect::<Html>()
                                    }
                                </div>
                            </>
                        }
                    }).collect::<Html>()
                }
            </div>
        }
    }

    fn view_move(&self, ctx: &Context<Self>, mov: Move) -> Html {
        html! {
            <div
                class="move"
                onclick={ctx.props().on_change.reform(move |_| StateChange::Move(mov))}
            >
                <span class={classes!("player", format!("p-{}", mov.player))}></span>
                <span>{ direction_to_arrow(mov.src, mov.dst) }</span>
                <span class="build">{ '\u{1F3E0}' }</span>
                <span>{ direction_to_arrow(mov.dst, mov.build) }</span>
            </div>
        }
    }

    fn view_side_move(
        &self,
        ctx: &Context<Self>,
        mov: &Move,
        idx_sug: usize,
        idx_next: usize,
    ) -> Html {
        html! {
            <span
                class="move"
                onclick={ctx.link().callback(move |_| SuggestMsg::MoveMultiple(idx_sug, idx_next))}
            >
                { direction_to_arrow(mov.src, mov.dst) }
                { direction_to_arrow(mov.dst, mov.build) }
            </span>
        }
    }
}

fn direction_to_arrow(src: u8, dst: u8) -> char {
    match (dst as i8) - (src as i8) {
        -6 => '\u{2196}',
        -5 => '\u{2191}',
        -4 => '\u{2197}',
        -1 => '\u{2190}',
        1 => '\u{2192}',
        4 => '\u{2199}',
        5 => '\u{2193}',
        6 => '\u{2198}',
        _ => unreachable!(),
    }
}
