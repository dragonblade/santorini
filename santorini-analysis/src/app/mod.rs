use std::cmp::Ordering;
use std::rc::Rc;

use yew::prelude::*;

use santorini_game::{Move, State, TreeNode};

mod history;
mod suggest;

use history::History;
use suggest::Suggest;

use super::board::Board;

pub struct App {
    tree_root: TreeNode,
    tree_current: TreeNode,
    state: Rc<State>,
    dialog_import: bool,
    textarea_state: NodeRef,
    textarea_tree: NodeRef,
}

pub enum Msg {
    StateChange(StateChange),
    BoardScroll(WheelEvent),
    MoveFirst,
    MovePrev,
    MoveNext,
    MoveLast,
    ToggleImport,
    CopyState,
    ImportState,
    CopyTree,
    ImportTree,
}

pub enum StateChange {
    Move(Move),
    Multiple(Vec<Move>),
    Node(TreeNode),
}

impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        let tree_root = TreeNode::new(State::new());
        let state = Rc::new(tree_root.state().clone());

        Self {
            tree_current: tree_root.clone(),
            tree_root,
            state,
            dialog_import: false,
            textarea_state: NodeRef::default(),
            textarea_tree: NodeRef::default(),
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::StateChange(chg) => {
                self.tree_current = match chg {
                    StateChange::Move(mov) => self.tree_current.add_move(mov),
                    StateChange::Multiple(moves) => {
                        let mut moves = moves.into_iter();
                        let mut tree_current = self.tree_current.clone();
                        while let Some(mov) = moves.next() {
                            tree_current = tree_current.add_move(mov);
                        }
                        tree_current
                    }
                    StateChange::Node(node) => node,
                };
                *Rc::make_mut(&mut self.state) = self.tree_current.state().clone();
                true
            }
            Msg::BoardScroll(ev) => {
                let node = match ev.delta_y().total_cmp(&0f64) {
                    Ordering::Less => self.tree_current.node_up(),
                    Ordering::Greater => self.tree_current.node_down(),
                    Ordering::Equal => None,
                };

                if let Some(node) = node {
                    self.tree_current = node;
                    *Rc::make_mut(&mut self.state) = self.tree_current.state().clone();
                }

                true
            }
            Msg::MoveFirst => {
                self.tree_current = self.tree_current.node_first();
                *Rc::make_mut(&mut self.state) = self.tree_current.state().clone();
                true
            }
            Msg::MovePrev => {
                if let Some(node) = self.tree_current.node_up() {
                    self.tree_current = node;
                    *Rc::make_mut(&mut self.state) = self.tree_current.state().clone();
                }
                true
            }
            Msg::MoveNext => {
                if let Some(node) = self.tree_current.node_down() {
                    self.tree_current = node;
                    *Rc::make_mut(&mut self.state) = self.tree_current.state().clone();
                }
                true
            }
            Msg::MoveLast => {
                self.tree_current = self.tree_current.node_last();
                *Rc::make_mut(&mut self.state) = self.tree_current.state().clone();
                true
            }
            Msg::ToggleImport => {
                self.dialog_import = !self.dialog_import;
                true
            }
            Msg::CopyState => {
                if let Some(node) = self.textarea_state.cast::<web_sys::HtmlInputElement>() {
                    node.select();
                }
                false
            }
            Msg::ImportState => {
                self.dialog_import = false;
                if let Some(node) = self.textarea_state.cast::<web_sys::HtmlInputElement>() {
                    let text = node.value();
                    if let Ok(state) = text.parse::<State>() {
                        self.tree_root = TreeNode::new(state.clone());
                        self.tree_current = self.tree_root.clone();
                        *Rc::make_mut(&mut self.state) = state;
                    }
                }
                true
            }
            Msg::CopyTree => {
                if let Some(node) = self.textarea_tree.cast::<web_sys::HtmlInputElement>() {
                    node.select();
                }
                false
            }
            Msg::ImportTree => {
                self.dialog_import = false;
                if let Some(node) = self.textarea_tree.cast::<web_sys::HtmlInputElement>() {
                    let text = node.value();
                    if let Ok(tree) = text.parse::<TreeNode>() {
                        self.tree_root = tree.clone();
                        self.tree_current = tree.node_last();
                        *Rc::make_mut(&mut self.state) = self.tree_current.state().clone();
                    }
                }
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let state = self.state.clone();
        html! {
            <ContextProvider<Rc<State>> context={state}>
                <main class="app">
                    <Board
                        on_move={ctx.link().callback(|m| Msg::StateChange(StateChange::Move(m)))}
                        on_wheel={ctx.link().callback(Msg::BoardScroll)}
                    >
                    </Board>
                    <div class="analysis">
                        <Suggest
                            on_change={ctx.link().callback(Msg::StateChange)}
                        >
                        </Suggest>
                        <History
                            tree_root={self.tree_root.clone()}
                            tree_current={self.tree_current.clone()}
                            on_node={ctx.link().callback(|n| Msg::StateChange(StateChange::Node(n)))}
                        >
                        </History>
                        <div class="toolbar">
                            <div class="tool" onclick={ctx.link().callback(|_| Msg::ToggleImport)}>{ "\u{1F4C3}" }</div>
                            <div class="set">
                                <div class="tool" onclick={ctx.link().callback(|_| Msg::MoveFirst)}>{ "\u{23EE}" }</div>
                                <div class="tool" onclick={ctx.link().callback(|_| Msg::MovePrev)}>{ "\u{23EA}" }</div>
                                <div class="tool" onclick={ctx.link().callback(|_| Msg::MoveNext)}>{ "\u{23E9}" }</div>
                                <div class="tool" onclick={ctx.link().callback(|_| Msg::MoveLast)}>{ "\u{23ED}" }</div>
                            </div>
                        </div>
                    </div>
                </main>
                <dialog open={self.dialog_import}>
                    <article>
                        <header>
                            <button class="close" rel="prev" onclick={ctx.link().callback(|_| Msg::ToggleImport)}></button>
                            <h3>{ "Import/export" }</h3>
                        </header>
                        <p class="import">
                            <label for="ser_state">{ "State" }</label>
                            <textarea ref={&self.textarea_state} id="ser_state" value={self.state.to_string()}></textarea>
                            <p class="buttons">
                                <button onclick={ctx.link().callback(|_| Msg::CopyState)}>{ "Select" }</button>
                                <button onclick={ctx.link().callback(|_| Msg::ImportState)}>{ "Import" }</button>
                            </p>
                            <label for="ser_tree">{ "Analysis" }</label>
                            <textarea ref={&self.textarea_tree} id="ser_tree" value={self.tree_root.to_string()}></textarea>
                            <p class="buttons">
                                <button onclick={ctx.link().callback(|_| Msg::CopyTree)}>{ "Select" }</button>
                                <button onclick={ctx.link().callback(|_| Msg::ImportTree)}>{ "Import" }</button>
                            </p>
                        </p>
                    </article>
                </dialog>
            </ContextProvider<Rc<State>>>
        }
    }
}
