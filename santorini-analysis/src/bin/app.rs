#![recursion_limit = "1024"]

use log::Level;

use santorini_analysis::App;

fn main() {
    console_error_panic_hook::set_once();
    console_log::init_with_level(Level::Debug).expect("error initializing log");

    yew::Renderer::<App>::new().render();
}
