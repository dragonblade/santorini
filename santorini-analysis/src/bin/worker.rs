use log::Level;
use yew_agent::Registrable;

use santorini_analysis::Worker;

fn main() {
    console_error_panic_hook::set_once();
    console_log::init_with_level(Level::Debug).expect("error initializing log");

    Worker::registrar().register();
}
