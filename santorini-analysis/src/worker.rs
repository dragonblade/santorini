use serde::{Deserialize, Serialize};
use yew_agent::worker::{HandlerId, WorkerScope};

use santorini_game::{Mcts, MctsOptions, State, Suggestion};

const MAX_NODES: usize = 8_000_000;

pub struct Worker {
    mcts: Mcts,
    total_runs: usize,
    total_duration: f64,
    previous_speed: f64,
}

#[allow(clippy::large_enum_variant)]
#[derive(Debug, Deserialize, Serialize)]
pub enum WorkerInput {
    New(State),
    Continue,
}

#[derive(Deserialize, Serialize)]
pub enum WorkerOutput {
    Results {
        suggestions: Vec<Suggestion>,
        progress: f64,
        speed: f64,
        depth: usize,
    },
    Stopped,
}

impl yew_agent::worker::Worker for Worker {
    type Input = WorkerInput;
    type Message = ();
    type Output = WorkerOutput;

    fn create(_scope: &WorkerScope<Self>) -> Self {
        let mcts = Mcts::with_options(State::new(), MctsOptions::default());

        Self {
            mcts,
            total_runs: 0,
            total_duration: 0f64,
            previous_speed: 0f64,
        }
    }

    fn update(&mut self, _scope: &WorkerScope<Self>, _msg: Self::Message) {}

    fn received(&mut self, scope: &WorkerScope<Self>, msg: Self::Input, id: HandlerId) {
        match msg {
            WorkerInput::New(state) => {
                if state.is_game_end() {
                    scope.respond(id, WorkerOutput::Stopped);
                    return;
                }

                self.mcts.reset(state);
                self.total_runs = 0;
                self.total_duration = 0f64;

                self.run(100);
                self.respond(scope, id);
            }
            WorkerInput::Continue => {
                if self.mcts.node_count() >= MAX_NODES {
                    scope.respond(id, WorkerOutput::Stopped);
                    return;
                }

                let runs = (self.previous_speed * 500f64) as usize;

                self.run(runs);
                self.respond(scope, id);
            }
        }
    }
}

impl Worker {
    fn run(&mut self, runs: usize) {
        use wasm_bindgen::JsCast;
        let performance = js_sys::eval("performance")
            .unwrap()
            .dyn_into::<web_sys::Performance>()
            .unwrap();
        let start = performance.now();
        for _ in 0..runs {
            self.mcts.run_round();
        }
        let end = performance.now();

        self.total_runs += runs;
        self.total_duration += end - start;
        self.previous_speed = runs as f64 / (end - start).max(1f64);
    }

    fn respond(&self, scope: &WorkerScope<Self>, id: HandlerId) {
        scope.respond(
            id,
            WorkerOutput::Results {
                suggestions: self.mcts.suggestions(),
                progress: (self.mcts.node_count() as f64) / (MAX_NODES as f64) * 100f64,
                speed: (self.total_runs as f64) / (self.total_duration / 1_000f64),
                depth: self.mcts.depth(),
            },
        );
    }
}
