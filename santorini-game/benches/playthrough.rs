use criterion::{criterion_group, criterion_main, Criterion};
use pprof::criterion::{Output, PProfProfiler};
use rand::prelude::*;

use santorini_game::State;

pub fn criterion_benchmark(c: &mut Criterion) {
    let mut rng = thread_rng();
    c.bench_function("playthrough", |b| {
        b.iter(|| {
            let mut state = State::new();
            while let Some(mov) = state.legal_moves_iter().choose(&mut rng) {
                state.mut_move(mov);
            }
        })
    });
}

criterion_group! {
    name = benches;
    config = Criterion::default().with_profiler(PProfProfiler::new(10_000, Output::Flamegraph(None)));
    targets = criterion_benchmark
}
criterion_main!(benches);
