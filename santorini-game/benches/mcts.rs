use criterion::{criterion_group, criterion_main, Criterion};
use pprof::criterion::{Output, PProfProfiler};

use santorini_game::{Mcts, State};

pub fn criterion_benchmark(c: &mut Criterion) {
    let mut mcts = Mcts::new(State::new());
    c.bench_function("mcts", |b| b.iter(|| mcts.run_round()));
}

criterion_group! {
    name = benches;
    config = Criterion::default().with_profiler(PProfProfiler::new(10_000, Output::Flamegraph(None)));
    targets = criterion_benchmark
}
criterion_main!(benches);
