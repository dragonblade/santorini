use std::fmt;
use std::str::FromStr;

use serde::{Deserialize, Serialize};

const MOVES_ALLOC: usize = 64;
const LAYER_DOME: usize = 3;
const LAYER_TOP: usize = 2;

const MASK_BOARD: u32 = 0x1FFFFFF; // 0b11111_11111_11111_11111_11111
const MASK_NOTCOL1: u32 = MASK_BOARD ^ 0x0108421; // 0b00001_00001_00001_00001_00001
const MASK_NOTCOL5: u32 = MASK_BOARD ^ 0x1084210; // 0b10000_10000_10000_10000_10000

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub struct State {
    board: [u32; 4],
    players: [u32; 2],
    turn: u8,
}

#[derive(Clone, Copy, Debug, Deserialize, Serialize, PartialEq)]
pub struct Move {
    pub player: u8,
    pub src: u8,
    pub dst: u8,
    pub build: u8,
}

impl State {
    pub fn new() -> Self {
        State {
            board: Default::default(),
            players: [0x40040, 0x10100],
            turn: 0,
        }
    }

    pub fn current_turn(&self) -> usize {
        self.turn as usize
    }

    pub fn view_players(&self) -> [Option<usize>; 25] {
        let mut res = [None; 25];
        for (p, player) in self.players.iter().enumerate() {
            for i in 0..25 {
                if (player >> i) & 1 == 1 {
                    res[i] = Some(p);
                }
            }
        }
        res
    }

    pub fn view_buildings(&self) -> [[bool; 4]; 25] {
        let mut res = [[false; 4]; 25];
        for (l, layer) in self.board.iter().enumerate() {
            for i in 0..25 {
                if (layer >> i) & 1 == 1 {
                    res[i][l] = true;
                }
            }
        }
        res
    }

    pub fn is_game_end(&self) -> bool {
        self.legal_moves_count() == 0
    }

    pub fn winner(&self) -> Option<usize> {
        if self.legal_moves_count() == 0 {
            return Some((self.turn as usize + 1) % self.players.len());
        }
        for (idx, player) in self.players.iter().enumerate() {
            if player & self.winning_mask() > 0 {
                return Some(idx);
            }
        }
        None
    }

    pub fn legal_moves_vec(&self) -> Vec<Move> {
        if self.has_winner() {
            return vec![];
        }

        let mut moves = Vec::with_capacity(MOVES_ALLOC);
        let allowed_build = self.allowed_build();

        macro_rules! adj_call {
            ($func:ident!, $mask:expr) => {
                $func!(mask_nort($mask));
                $func!(mask_noea($mask));
                $func!(mask_east($mask));
                $func!(mask_soea($mask));
                $func!(mask_sout($mask));
                $func!(mask_sowe($mask));
                $func!(mask_west($mask));
                $func!(mask_nowe($mask));
            };
        }

        macro_rules! helper_a {
            ($src_mask:expr) => {{
                let src_mask = $src_mask;
                let src = src_mask.trailing_zeros() as u8;
                let allowed_dst = self.allowed_dst(src_mask);

                macro_rules! helper_b {
                    ($dst_mask:expr) => {{
                        let dst_mask = $dst_mask;
                        let dst = dst_mask.trailing_zeros() as u8;
                        let allowed_build = allowed_build ^ (src_mask | dst_mask);

                        macro_rules! helper_c {
                            ($build_mask:expr) => {
                                let build_mask = $build_mask;
                                if build_mask & allowed_build > 0 {
                                    moves.push(Move {
                                        player: self.turn,
                                        src,
                                        dst,
                                        build: build_mask.trailing_zeros() as u8,
                                    });
                                }
                            };
                        }

                        if dst_mask & allowed_dst > 0 {
                            adj_call!(helper_c!, dst_mask);
                        }
                    }};
                }

                adj_call!(helper_b!, src_mask);
            }};
        }

        let player = self.players[self.turn as usize];
        let worker = 0x1u32 << player.trailing_zeros();
        helper_a!(worker);
        helper_a!(player ^ worker);

        moves
    }

    pub fn legal_moves_iter(&self) -> MoveGenerator {
        MoveGenerator::from(self)
    }

    pub fn legal_moves_count(&self) -> usize {
        self.legal_moves_iter().size_hint().0
    }

    pub fn mut_move(&mut self, mov: Move) {
        self.players[mov.player as usize] ^= (0x1 << mov.src) | (0x1 << mov.dst);
        let build_mask = 0x1 << mov.build;
        for layer in self.board.iter_mut() {
            if *layer & build_mask == 0 {
                *layer |= build_mask;
                break;
            }
        }

        self.turn = (self.turn + 1) % self.players.len() as u8;
    }

    fn has_winner(&self) -> bool {
        self.players_flattened() & self.winning_mask() > 0
    }

    fn players_flattened(&self) -> u32 {
        self.players.iter().fold(0, |a, b| a | b)
    }

    fn allowed_dst(&self, src_mask: u32) -> u32 {
        let mut iter = self.board.iter();
        while let Some(layer) = iter.next() {
            if layer & src_mask == 0 {
                break;
            }
        }
        MASK_BOARD
            ^ (iter.fold(0, |a, b| a | b) | self.board[LAYER_DOME] | self.players_flattened())
    }

    fn allowed_build(&self) -> u32 {
        MASK_BOARD ^ (self.board[LAYER_DOME] | self.players_flattened())
    }

    fn winning_mask(&self) -> u32 {
        self.board[LAYER_TOP] ^ self.board[LAYER_DOME]
    }
}

pub struct MoveGenerator<'a> {
    state: &'a State,
    workers: [(u32, u32); 2],
    allowed_build: u32,
    pos: Option<(usize, u8, u8)>, // (worker, dst_dir, build_dir)
}

impl<'a> MoveGenerator<'a> {
    fn from(state: &'a State) -> Self {
        let player = state.players[state.turn as usize];
        let worker_a = 0x1u32 << player.trailing_zeros();
        let worker_b = player ^ worker_a;

        let pos = if state.has_winner() {
            Some((usize::MAX, 0, 0))
        } else {
            None
        };

        Self {
            state,
            workers: [
                (worker_a, state.allowed_dst(worker_a)),
                (worker_b, state.allowed_dst(worker_b)),
            ],
            allowed_build: state.allowed_build(),
            pos,
        }
    }
}

impl<'a> Iterator for MoveGenerator<'a> {
    type Item = Move;

    fn size_hint(&self) -> (usize, Option<usize>) {
        if self.pos.is_some() {
            return (0, None);
        }

        let mut moves = 0;

        macro_rules! helper_a {
            ($worker:expr) => {{
                let (src_mask, allowed_dst) = $worker;

                macro_rules! helper_b {
                    ($dst_mask:expr) => {
                        let dst_mask = $dst_mask;
                        if dst_mask & allowed_dst > 0 {
                            let allowed_build = self.allowed_build ^ (src_mask | dst_mask);
                            let mask_build = mask_around(dst_mask) & allowed_build;
                            moves += mask_build.count_ones() as usize;
                        }
                    };
                }

                helper_b!(mask_nort(src_mask));
                helper_b!(mask_noea(src_mask));
                helper_b!(mask_east(src_mask));
                helper_b!(mask_soea(src_mask));
                helper_b!(mask_sout(src_mask));
                helper_b!(mask_sowe(src_mask));
                helper_b!(mask_west(src_mask));
                helper_b!(mask_nowe(src_mask));
            }};
        }

        helper_a!(self.workers[0]);
        helper_a!(self.workers[1]);

        (moves, Some(moves))
    }

    fn nth(&mut self, n: usize) -> Option<Self::Item> {
        #[inline(always)]
        fn mask_dir(mask: u32, dir: u8) -> u32 {
            match dir {
                0 => mask_nort(mask),
                1 => mask_noea(mask),
                2 => mask_east(mask),
                3 => mask_soea(mask),
                4 => mask_sout(mask),
                5 => mask_sowe(mask),
                6 => mask_west(mask),
                7 => mask_nowe(mask),
                _ => unreachable!(),
            }
        }

        let mut n = n + 1;
        let (mut worker, mut dst_dir, mut build_dir) = self.pos.unwrap_or_default();

        while worker < self.workers.len() {
            let (src_mask, allowed_dst) = self.workers[worker];

            while dst_dir < 8 {
                let dst_mask = mask_dir(src_mask, dst_dir);

                if dst_mask & allowed_dst > 0 {
                    let allowed_build = self.allowed_build ^ (src_mask | dst_mask);
                    let count_legal = (mask_around(dst_mask) & allowed_build).count_ones() as usize;

                    if build_dir == 0 && count_legal < n {
                        n -= count_legal;
                    } else {
                        while build_dir < 8 {
                            let build_mask = mask_dir(dst_mask, build_dir);
                            build_dir += 1;

                            if build_mask & allowed_build > 0 {
                                n -= 1;
                            }
                            if n == 0 {
                                self.pos = Some((worker, dst_dir, build_dir));
                                return Some(Move {
                                    player: self.state.turn,
                                    src: src_mask.trailing_zeros() as u8,
                                    dst: dst_mask.trailing_zeros() as u8,
                                    build: build_mask.trailing_zeros() as u8,
                                });
                            }
                        }
                    }
                }

                dst_dir += 1;
                build_dir = 0;
            }

            worker += 1;
            dst_dir = 0;
            build_dir = 0;
        }

        None
    }

    fn next(&mut self) -> Option<Self::Item> {
        self.nth(0)
    }
}

#[inline(always)]
fn mask_around(mask: u32) -> u32 {
    let line = mask_west(mask) | mask | mask_east(mask);
    mask_nort(line) | line | mask_sout(line)
}

#[inline(always)]
fn mask_nort(mask: u32) -> u32 {
    mask >> 5
}

#[inline(always)]
fn mask_sout(mask: u32) -> u32 {
    mask << 5 & MASK_BOARD
}

#[inline(always)]
fn mask_east(mask: u32) -> u32 {
    (mask << 1) & MASK_NOTCOL1
}

#[inline(always)]
fn mask_noea(mask: u32) -> u32 {
    (mask << 6) & MASK_NOTCOL1
}

#[inline(always)]
fn mask_soea(mask: u32) -> u32 {
    (mask >> 4) & MASK_NOTCOL1
}

#[inline(always)]
fn mask_west(mask: u32) -> u32 {
    (mask >> 1) & MASK_NOTCOL5
}

#[inline(always)]
fn mask_sowe(mask: u32) -> u32 {
    (mask >> 6) & MASK_NOTCOL5
}

#[inline(always)]
fn mask_nowe(mask: u32) -> u32 {
    (mask << 4) & MASK_NOTCOL5
}

impl fmt::Display for State {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fn write_mask(f: &mut fmt::Formatter<'_>, pos: u32) -> fmt::Result {
            let pos = pos.trailing_zeros();
            const COLUMNS: [char; 5] = ['a', 'b', 'c', 'd', 'e'];
            let row = pos / 5;
            let col = pos % 5;
            write!(f, "{}{}", COLUMNS[col as usize], row + 1)
        }

        for y in 0..5 {
            let mut line: u32 = 0;
            for (l, layer) in self.board.iter().enumerate() {
                let mask = 0x1 << l;
                for x in 0..5 {
                    if layer & (0x1 << (y * 5 + x)) > 0 {
                        line |= mask << (x * 4);
                    }
                }
            }

            if y > 0 {
                f.write_str("/")?;
            }
            write!(f, "{:x}", line)?;
        }

        for player in self.players {
            let worker = 0x1 << player.trailing_zeros();
            f.write_str(" ")?;
            write_mask(f, worker)?;
            write_mask(f, player ^ worker)?;
        }

        write!(f, " {}", self.turn)
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct ParseStateError;

impl FromStr for State {
    type Err = ParseStateError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        fn string_to_board(s: &str) -> Result<[u32; 4], ParseStateError> {
            let mut board = [0; 4];
            let mut lines = s.split('/');

            for r in 0..5 {
                let line = lines.next().ok_or(ParseStateError)?;
                let line = u32::from_str_radix(line, 16).map_err(|_| ParseStateError)?;

                for l in 0..4 {
                    let mask = 0x1 << l;

                    for c in 0..5 {
                        if line & (mask << c * 4) > 0 {
                            board[l] |= 0x1 << (r * 5 + c);
                        }
                    }
                }
            }

            Ok(board)
        }

        fn string_to_player(s: &str) -> Result<u32, ParseStateError> {
            if s.len() != 4 {
                return Err(ParseStateError);
            }
            let (a, b) = s.split_at(2);
            let a = 0x1 << string_to_pos(a)?;
            let b = 0x1 << string_to_pos(b)?;
            Ok(a | b)
        }

        fn string_to_pos(s: &str) -> Result<u8, ParseStateError> {
            if s.len() != 2 {
                return Err(ParseStateError);
            }
            let (col, row) = s.split_at(1);
            let col = match col {
                "a" => 0,
                "b" => 1,
                "c" => 2,
                "d" => 3,
                "e" => 4,
                _ => return Err(ParseStateError),
            };
            let row = row.parse::<u8>().map_err(|_| ParseStateError)?;
            if row < 1 || row > 5 {
                return Err(ParseStateError);
            }

            Ok((row - 1) * 5 + col)
        }

        let mut iter = s.split_whitespace();

        let board = string_to_board(iter.next().ok_or(ParseStateError)?)?;
        let player_1 = string_to_player(iter.next().ok_or(ParseStateError)?)?;
        let player_2 = string_to_player(iter.next().ok_or(ParseStateError)?)?;
        let turn = iter
            .next()
            .ok_or(ParseStateError)?
            .parse::<u8>()
            .map_err(|_| ParseStateError)?;

        if iter.next().is_some() {
            Err(ParseStateError)
        } else {
            Ok(State {
                board,
                players: [player_1, player_2],
                turn,
            })
        }
    }
}

#[cfg(test)]
mod tests {
    use rand::prelude::*;

    use super::*;

    #[test]
    fn legal_moves_vec_iter() {
        let mut state = State::new();

        while !state.is_game_end() {
            let legal_moves_vec = state.legal_moves_vec();
            let legal_moves_iter = {
                let (num, _) = state.legal_moves_iter().size_hint();
                (0..num)
                    .map(|n| state.legal_moves_iter().nth(n).unwrap())
                    .collect::<Vec<_>>()
            };

            legal_moves_vec
                .iter()
                .for_each(|m| assert!(legal_moves_iter.contains(m)));
            legal_moves_iter
                .iter()
                .for_each(|m| assert!(legal_moves_vec.contains(m)));

            state.mut_move(state.legal_moves_iter().choose(&mut thread_rng()).unwrap());
        }
    }

    #[test]
    fn legal_moves_vec_iter_x2() {
        let mut state = State::new();

        while !state.is_game_end() {
            let legal_moves_vec = state.legal_moves_vec();
            let legal_moves_iter = {
                let (num, _) = state.legal_moves_iter().size_hint();
                (0..num)
                    .step_by(2)
                    .flat_map(|n| {
                        let mut iter = state.legal_moves_iter();
                        let mut vec = vec![iter.nth(n).unwrap()];
                        if n + 1 < num {
                            vec.push(iter.next().unwrap());
                        }
                        vec
                    })
                    .collect::<Vec<_>>()
            };

            legal_moves_vec
                .iter()
                .for_each(|m| assert!(legal_moves_iter.contains(m)));
            legal_moves_iter
                .iter()
                .for_each(|m| assert!(legal_moves_vec.contains(m)));

            state.mut_move(state.legal_moves_iter().choose(&mut thread_rng()).unwrap());
        }
    }

    #[test]
    fn print_and_parse() {
        let str = "0/0/77000/f1f00/11101 e3d4 b5e5 1";

        let state = str.parse::<State>().unwrap();
        assert_eq!(
            state,
            State {
                board: [0x1DE6000, 0xA6000, 0xA6000, 0xA0000,],
                players: [0x44000, 0x1200000,],
                turn: 1,
            }
        );

        assert_eq!(str, state.to_string());
    }
}
