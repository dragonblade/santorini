use std::cell::{Ref, RefCell};
use std::fmt;
use std::rc::{Rc, Weak};
use std::str::FromStr;

use super::{Move, State};

#[derive(Clone, Debug, PartialEq)]
pub struct TreeNode(Rc<RefCell<Node>>);

#[derive(Debug)]
pub struct Node {
    state: State,
    parent: Weak<RefCell<Node>>,
    children: Vec<(Move, TreeNode)>,
}

impl TreeNode {
    pub fn new(state: State) -> Self {
        Self(Rc::new(RefCell::new(Node {
            state,
            parent: Weak::new(),
            children: vec![],
        })))
    }

    pub fn state(&self) -> Ref<'_, State> {
        Ref::map(self.0.borrow(), |n| &n.state)
    }

    pub fn children(&self) -> Ref<'_, Vec<(Move, TreeNode)>> {
        Ref::map(self.0.borrow(), |n| &n.children)
    }

    pub fn node_up(&self) -> Option<Self> {
        self.0.borrow().parent.upgrade().map(Self)
    }

    pub fn node_down(&self) -> Option<Self> {
        self.0.borrow().children.first().map(|(_, n)| n.clone())
    }

    pub fn node_first(&self) -> Self {
        let mut node = self.clone();
        while let Some(parent) = node.node_up() {
            node = parent;
        }
        node
    }

    pub fn node_last(&self) -> Self {
        let mut node = self.clone();
        while let Some(child) = node.node_down() {
            node = child;
        }
        node
    }

    pub fn add_move(&self, mov: Move) -> Self {
        let mut state = self.state().clone();
        state.mut_move(mov);

        self.get_or_add_child(mov, state)
    }

    fn get_or_add_child(&self, mov: Move, state: State) -> Self {
        let mut node = self.0.borrow_mut();
        if let Some((_, node)) = node.children.iter().find(|(m, _)| m == &mov) {
            node.clone()
        } else {
            let new = TreeNode(Rc::new(RefCell::new(Node {
                state,
                parent: Rc::downgrade(&self.0),
                children: vec![],
            })));
            node.children.push((mov, new.clone()));
            new
        }
    }
}

impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        self.state == other.state && self.children == other.children
    }
}

impl fmt::Display for TreeNode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fn write_tree(f: &mut fmt::Formatter<'_>, tree: TreeNode) -> fmt::Result {
            let mut current = Some(tree);

            while let Some(node) = current.take() {
                let children = node.children();
                let mut children = children.iter();

                if let Some((mov, next)) = children.next() {
                    f.write_str(" ")?;
                    write_move(f, mov)?;
                    current = Some(next.clone());
                }

                for (mov, side) in children {
                    f.write_str(" (")?;
                    write_move(f, mov)?;
                    write_tree(f, side.clone())?;
                    f.write_str(")")?;
                }
            }

            Ok(())
        }

        fn write_move(f: &mut fmt::Formatter<'_>, mov: &Move) -> fmt::Result {
            write_pos(f, mov.src)?;
            write_pos(f, mov.dst)?;
            write_pos(f, mov.build)
        }

        fn write_pos(f: &mut fmt::Formatter<'_>, pos: u8) -> fmt::Result {
            const COLUMNS: [char; 5] = ['a', 'b', 'c', 'd', 'e'];
            let row = pos / 5;
            let col = pos % 5;
            write!(f, "{}{}", COLUMNS[col as usize], row + 1)
        }

        if self.0.borrow().state != State::new() {
            write!(f, "[{}] ", self.state())?;
        }

        let children = self.children();
        let mut children = children.iter();

        if let Some((mov, child)) = children.next() {
            write_move(f, mov)?;

            for (mov, side) in children {
                f.write_str(" (")?;
                write_move(f, mov)?;
                write_tree(f, side.clone())?;
                f.write_str(")")?;
            }

            write_tree(f, child.clone())?;
        }

        Ok(())
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct ParseTreeNodeError;

impl FromStr for TreeNode {
    type Err = ParseTreeNodeError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use std::iter::Peekable;
        use std::str::CharIndices;

        enum Token<'a> {
            Ident(&'a str),
            ParOpen,
            ParClose,
        }

        struct Tokenizer<'a> {
            s: &'a str,
            i: Peekable<CharIndices<'a>>,
        }

        impl<'a> Iterator for Tokenizer<'a> {
            type Item = Token<'a>;

            fn next(&mut self) -> Option<Self::Item> {
                while let Some((i, c)) = self.i.next() {
                    match c {
                        '(' => return Some(Token::ParOpen),
                        ')' => return Some(Token::ParClose),
                        c if c.is_alphanumeric() => {
                            let start = i;
                            while self
                                .i
                                .peek()
                                .map(|(_, c)| c.is_alphanumeric())
                                .unwrap_or(false)
                            {
                                self.i.next();
                            }
                            let end = self
                                .i
                                .peek()
                                .map(|(i, _)| *i)
                                .unwrap_or_else(|| self.s.len());
                            return Some(Token::Ident(&self.s[start..end]));
                        }
                        _ => {}
                    }
                }

                None
            }
        }

        fn string_append_node<'a>(
            mut tokens: Tokenizer<'a>,
            mut node: TreeNode,
        ) -> Result<Tokenizer<'a>, ParseTreeNodeError> {
            while let Some(t) = tokens.next() {
                match t {
                    Token::ParClose => return Ok(tokens),
                    Token::ParOpen => {
                        tokens =
                            string_append_node(tokens, node.node_up().ok_or(ParseTreeNodeError)?)?;
                    }
                    Token::Ident(ident) => {
                        let mov = string_to_move(ident, &node.state())?;
                        node = node.add_move(mov);
                    }
                }
            }
            Ok(tokens)
        }

        fn string_to_move(s: &str, state: &State) -> Result<Move, ParseTreeNodeError> {
            if s.len() != 6 {
                return Err(ParseTreeNodeError);
            }
            let mov = Move {
                player: state.current_turn() as u8,
                src: string_to_pos(&s[0..2])?,
                dst: string_to_pos(&s[2..4])?,
                build: string_to_pos(&s[4..6])?,
            };
            if !state.legal_moves_vec().contains(&mov) {
                return Err(ParseTreeNodeError);
            }
            Ok(mov)
        }

        fn string_to_pos(s: &str) -> Result<u8, ParseTreeNodeError> {
            if s.len() != 2 {
                return Err(ParseTreeNodeError);
            }
            let (col, row) = s.split_at(1);
            let col = match col {
                "a" => 0,
                "b" => 1,
                "c" => 2,
                "d" => 3,
                "e" => 4,
                _ => return Err(ParseTreeNodeError),
            };
            let row = row.parse::<u8>().map_err(|_| ParseTreeNodeError)?;
            if row < 1 || row > 5 {
                return Err(ParseTreeNodeError);
            }

            Ok((row - 1) * 5 + col)
        }

        let s = s.trim_start();
        let (s, state) = if s.starts_with('[') {
            let (s, rest) = &s[1..].split_once(']').ok_or(ParseTreeNodeError)?;
            (
                &rest[1..],
                s.parse::<State>().map_err(|_| ParseTreeNodeError)?,
            )
        } else {
            (s, State::new())
        };

        let root = TreeNode::new(state);
        let mut res = string_append_node(
            Tokenizer {
                i: s.char_indices().peekable(),
                s,
            },
            root.clone(),
        )?;
        match res.next() {
            Some(_) => Err(ParseTreeNodeError),
            None => Ok(root),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse() {
        let str = "d4c3c4 d2d3e4 c3c4d4 d3e4e5 b2c3d3 e4e5e4
        c4d4e4 b4c5d5 c3d3e3 c5d5c5 d3d2e3 d5c5c4 d2d3c4 (d2d3d2
        c5d5e4 d3e3d2 d5c4c3 d4d3e2 e5d5c5 d3d4c3 c4c3c2) e5d5c4
        (c5b4c4 d3e3d3 e5d5e4 e3d3e3 b4c3d2 (b4c3c2 d3e3d2) d3e3d2
        (d3e3d3) (d3e3e2)) (e5d5e4 d3e3d3 c5b4c4 e3d3e3 b4c3d2
        d3e3d3) d3e3d3 d5e5e4 e3d3e3 c5b5a5 d3e3d3";
        assert!(str.parse::<TreeNode>().is_ok());
    }

    #[test]
    fn parse_with_state() {
        let str = "[0/0/77000/f1f00/11101 d1e1 a1b1 1] b1b2a3 (b1a2b3) d1e2d2";
        let tree = str.parse::<TreeNode>().unwrap();
        assert_eq!(str, tree.to_string());
    }

    #[test]
    fn print_and_parse() {
        let tree = TreeNode::new(State::new());
        {
            let m1 = tree.add_move(Move {
                player: 0,
                src: 6,
                dst: 2,
                build: 7,
            });
            let m2 = m1.add_move(Move {
                player: 1,
                src: 8,
                dst: 7,
                build: 6,
            });
            let _m3 = m2.add_move(Move {
                player: 0,
                src: 2,
                dst: 3,
                build: 2,
            });
            let m2a1 = m1.add_move(Move {
                player: 1,
                src: 8,
                dst: 9,
                build: 14,
            });
            let _m2a2 = m2a1.add_move(Move {
                player: 0,
                src: 18,
                dst: 14,
                build: 13,
            });
            let _m2a1a1 = m2a1.add_move(Move {
                player: 0,
                src: 18,
                dst: 14,
                build: 19,
            });
            let _m2b2 = m1.add_move(Move {
                player: 1,
                src: 16,
                dst: 15,
                build: 20,
            });
        }

        let str = tree.to_string();
        assert_eq!(
            str,
            "b2c1c2 d2c2b2 (d2e2e3 d4e3d3 (d4e3e4)) (b4a4a5) c1d1c1"
        );

        let parsed: TreeNode = str.parse().unwrap();
        assert_eq!(tree, parsed);
    }
}
