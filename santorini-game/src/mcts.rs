use std::cell::RefCell;
use std::rc::{Rc, Weak};

use rand::prelude::*;
use rand_xoshiro::Xoshiro256PlusPlus;
use serde::{Deserialize, Serialize};

use super::{Move, State};

pub struct Mcts {
    rng: Xoshiro256PlusPlus,
    options: MctsOptions,
    root: NodeChild,
    depth: usize,
}

pub struct MctsOptions {
    pub exploration_parameter: f64,
}

#[derive(Deserialize, Serialize)]
pub struct Suggestion {
    pub mov: Move,
    pub next: Vec<Move>,
    pub score: f64,
    pub win: f64,
}

type NodeChild = Rc<RefCell<Node>>;
type NodeParent = Weak<RefCell<Node>>;

struct Node {
    state: State,
    parent: NodeParent,
    children: Vec<(Move, NodeChild)>,
    unexplored: Vec<Move>,
    size: usize,
    wins: usize,
    plays: usize,
}

impl Default for MctsOptions {
    fn default() -> Self {
        Self {
            exploration_parameter: std::f64::consts::SQRT_2,
        }
    }
}

impl Mcts {
    pub fn new(state: State) -> Self {
        Self::with_options(state, Default::default())
    }

    pub fn with_options(state: State, options: MctsOptions) -> Self {
        assert!(!state.is_game_end());

        let root = Rc::new(RefCell::new(Node {
            unexplored: state.legal_moves_vec(),
            state,
            parent: Weak::new(),
            children: vec![],
            size: 1,
            wins: 0,
            plays: 0,
        }));

        Self {
            rng: Xoshiro256PlusPlus::from_rng(thread_rng()).expect("Could not initialize PRNG"),
            options,
            root,
            depth: 0,
        }
    }

    pub fn reset(&mut self, state: State) {
        if let Some((_, child)) = self
            .root
            .clone()
            .borrow()
            .children
            .iter()
            .find(|(_, child)| child.borrow().state == state)
        {
            self.root = child.clone();
            self.depth -= 1;
        } else {
            self.root = Rc::new(RefCell::new(Node {
                unexplored: state.legal_moves_vec(),
                state,
                parent: Weak::new(),
                children: vec![],
                size: 1,
                wins: 0,
                plays: 0,
            }));
            self.depth = 0;
        }
    }

    pub fn suggestions(&self) -> Vec<Suggestion> {
        let mut suggestions = self
            .root
            .borrow()
            .children
            .iter()
            .map(|(mov, child)| Suggestion {
                mov: *mov,
                next: {
                    let mut moves = Vec::with_capacity(self.depth);
                    let mut node = Some(child.clone());
                    while let Some((mov, child, _)) = node.and_then(|node| {
                        node.borrow()
                            .children
                            .iter()
                            .map(|(mov, child)| (*mov, child.clone(), child.borrow().plays))
                            .max_by_key(|(_, _, s)| *s)
                    }) {
                        moves.push(mov);
                        node = Some(child);
                    }
                    moves
                },
                score: (child.borrow().plays as f64) / (self.root.borrow().plays as f64),
                win: (child.borrow().wins as f64) / (child.borrow().plays as f64),
            })
            .collect::<Vec<_>>();
        suggestions.sort_by(|a, b| b.score.partial_cmp(&a.score).unwrap());
        suggestions
    }

    pub fn depth(&self) -> usize {
        self.depth
    }

    pub fn node_count(&self) -> usize {
        self.root.borrow().size
    }

    #[cfg(not(target_family = "wasm"))]
    pub fn run(&mut self) {
        use std::time::{Duration, Instant};

        let start = Instant::now();

        while start.elapsed() < Duration::from_secs(5) {
            self.run_round();
        }
    }

    pub fn run_round(&mut self) {
        let (leaf, depth) = self.select();
        let (expanded, node) = self.expand(leaf);
        let results = self.simulate(&node);
        self.backpropagate(node, expanded, results);
        self.depth = self.depth.max(depth);
    }

    fn select(&self) -> (NodeChild, usize) {
        let mut node = self.root.clone();
        let mut depth = 0;

        loop {
            if !node.borrow().unexplored.is_empty() || node.borrow().children.is_empty() {
                break;
            }

            // UCT formula: (w_i / n_i + c * sqrt(ln(N_i) / n_i))

            let ln = (node.borrow().plays as f64).ln();

            let new = node
                .borrow()
                .children
                .iter()
                .map(|(_, child)| {
                    let wins = child.borrow().wins as f64;
                    let plays = child.borrow().plays as f64;
                    let uct =
                        (wins / plays) + (self.options.exploration_parameter * (ln / plays).sqrt());
                    (child, uct)
                })
                .max_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap())
                .unwrap()
                .0
                .clone();

            node = new;
            depth += 1;
        }

        (node, depth)
    }

    fn expand(&mut self, node: NodeChild) -> (bool, NodeChild) {
        if node.borrow().unexplored.is_empty() {
            return (false, node);
        }

        let i = self.rng.gen_range(0..node.borrow().unexplored.len());
        let _move = node.borrow_mut().unexplored.swap_remove(i);
        let mut state = node.borrow().state.clone();
        state.mut_move(_move);

        let child = Rc::new(RefCell::new(Node {
            unexplored: state.legal_moves_vec(),
            state,
            parent: Rc::downgrade(&node),
            children: vec![],
            size: 1,
            wins: 0,
            plays: 0,
        }));
        node.borrow_mut().children.push((_move, child.clone()));
        (true, child)
    }

    fn simulate(&mut self, node: &NodeChild) -> [usize; 2] {
        let mut wins = [0; 2];

        let mut state = node.borrow().state.clone();
        while let Some(mov) = state.legal_moves_iter().choose(&mut self.rng) {
            state.mut_move(mov);
        }

        wins[state.winner().unwrap()] += 1;
        wins
    }

    fn backpropagate(&mut self, mut node: NodeChild, expanded: bool, wins: [usize; 2]) {
        loop {
            node.borrow_mut().plays += 1;
            if expanded {
                node.borrow_mut().size += 1;
            }
            let parent = node.borrow().parent.clone();
            match Weak::upgrade(&parent) {
                Some(parent) => {
                    node.borrow_mut().wins += wins[parent.borrow().state.current_turn()];
                    node = parent;
                }
                None => break,
            }
        }
    }
}
